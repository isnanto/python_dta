# numbers = [10, 5, 7, 2, 1]
# print("List Pertama :", numbers)  # printing original list content
# numbers[0] = 111
# print("setelah di tambahkan 111 pada posisi 0", numbers)

# print("Hapus elemen nomor 3 ")
# del(numbers[3])
# print("numbers setelah penghapusan elemen 3", numbers)
# print()

# print("Angka pertama adalah ",
#       numbers[0], " yang terakhir : ", numbers[len(numbers)-1])

# # penomoran elemen negatif menunjukan posisi dari posisi terakhir contoh
# print("elemen nomor -2 adalah ", numbers[-2])

# Lab
# hatList = [1, 2, 3, 4, 5]

# # input untuk menerima angka pengganti element tengah
# mid = int(input("Masukan angka pengganti elemen tengah "))
# hatList[len(hatList) % 2] = mid
# print("nilai setelah di ganti", hatList)
# # hapus elemen terakhir
# print("hapus elemen terakhir")
# del(hatList[-1])
# # cetak panjang list
# print("hatlist terakhir", hatList)

# List Methode
# step 1
# beatles = []
# print("Step 1:", beatles)

# # step 2
# beatles.append("John Lenon")
# beatles.append("Paul")
# beatles.append("George")
# print("Step 2:", beatles)

# # step 3
# for i in range(2):
#     beatles.append(input("Masukan nama kamu :"))
# print("Step 3:", beatles)

# # step 4
# del(beatles[-1])
# del(beatles[-1])
# print("Step 4:", beatles)

# # step 5
# beatles.insert(0, "Siti")
# print("Step 5:", beatles)


# # testing list legth
# print("The Fab", len(beatles))

# =====================================
# Take away
# =====================================
# 1. List adalah type data menyimpan multiple objek dipisahkan oleh koma
# 2. dapat di index dan di update
# 3. dapat berisi list juga (nested list)a=[a,1,c,["list",1,2,3],false]
# 4. bisa di delet (del(a[nomorelemen]))
# 5. dapat menjadi iterasi ( for i on a )
# 6. fungsi len dapat digunakan untuk mengecek jumlah elemen
