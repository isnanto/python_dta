# # Copying the whole list
# list1 = [1]
# list2 = list1[:]
# list1[0] = 2
# print(list2)

# # Copying part of the list
# myList = [10, 8, 6, 4, 2]
# newList = myList[1:]
# print(newList)

# myList = [1, 2, 4, 4, 1, 4, 2, 6, 2, 9]
# #
# # put your code here
# temp = []
# for n in myList:
#     if n not in temp:
#         temp.append(n)
# myList = temp
# #
# print("The list with unique elements only:")
# print(myList)

# a = [1 for n in range(3)]
# print(a)

# z = 10
# y = 0
# x = z > y or z == y
# print(x)
# i = 2
# while i >= 0:
#     print("*")
#     i -= 2

# lst = [0, 1, 2, 3]
# x = 1
# for elm in lst:
#     x *= elm
# print(x)

# for i in range(-1, 1):
#     print("#")

# nums = []
# vals = nums[:]
# vals.append(1)
# print(vals)
# print(nums)
