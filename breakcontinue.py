# largestNumber = -99999999
# counter = 0

# number = int(input("masukin nomor "))

# while number != -1:
#     if number == -1:
#         continue
#     counter += 1

#     if number > largestNumber:
#         largestNumber = number
#     number = int(input("Enter a number or type -1 to end program: "))

# if counter:
#     print("The largest number is", largestNumber)
# else:
#     print("You haven't entered any number.")

# break, continue
# Prompt the user to enter a word
# and assign it to the userWord variable.
userWord = input("masukan namamu : ").upper()

for letter in userWord:
    # Complete the body of the for loop.
    if letter == "A" or letter == "E" or letter == "I" or letter == "O" or letter == "U":
        continue
    print(letter)
