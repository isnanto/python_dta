def isYearLeap(year):
    # if year % 100 == 0:
    #     return False
    if year % 4 == 0:
        return True
    else:
        return False


def daysInMonth(year, month):
    if month > 12 or month < 1:
        return None

    if isYearLeap(year):
        if month == 2:
            return 29
    else:
        if month == 2:
            return 28
    if month <= 7:
        if month % 2 == 0:
            return 30
        else:
            return 31

    else:
        if month % 2 == 0:
            return 31
        else:
            return 30


def dayOfYear(year, month, day):
    hari = 0
    for n in range(1, month, 1):
        hari += daysInMonth(year, n)
    return hari + day


def isPrime(num):
    if num == 2:
        return True

    for n in range(2, num):
        if num % n == 0:
            return False
        else:
            return True


for i in range(1, 20):
    if isPrime(i + 1):
        print("nilai prime", i + 1, end=" ")
        print()

# testYears = [1900, 2000, 2016, 1987]
# testMonths = [2, 2, 1, 11]
# testResults = [28, 29, 31, 30]
# for i in range(len(testYears)):
#     yr = testYears[i]
#     mo = testMonths[i]
#     print(yr, mo, "->", end="")
#     result = daysInMonth(yr, mo)
#     if result == testResults[i]:
#         print("OK", result)
#     else:
#         print("Failed", result)

# print("jumlah hari", dayOfYear(2000, 12, 31))
