class Kardus:
    # atribut
    def __init__(self, panjang, lebar, tinggi):
        self.panjang = panjang
        self.lebar = lebar
        self.tinggi = tinggi

    def volume(self):
        return self.panjang * self.lebar * self.tinggi

    def luas(self):
        return 2*(self.panjang*self.lebar+self.panjang*self.tinggi+self.tinggi*self.lebar)

    def masajenis(self, massa):
        return massa/self.volume()


kardus = Kardus(5, 4, 2)
print(kardus.panjang)
print('Volume kardus adalah : ', kardus.volume())
print('luas kardus adalah :', kardus.luas())
print('masa jenis kardus adalah :', kardus.masajenis(50))
