import matplotlib.pyplot as plt

import random


def generateRandomSquarePoints(n, length, center=(0, 0)):
    # MULAI KODEMU DI SINI
    minx = center[0]-length/2
    miny = center[1]-length/2

    # Gunakan list comprehension dengan variable minx, miny, length, dan n
    points = [[random.uniform(minx, minx + length),
               random.uniform(miny, miny + length)] for x in range(n)]

    return points


# print(isPointInCircle(1, 1, 1, center=(0, 0)), isPointInCircle(1, 0, 1, center=(0, 0)),
#       isPointInCircle(1, 1, 1, center=(1, 0)), isPointInCircle(0, 0, 1, center=(1, 1)))
random.seed(0)

# generate 100 point di dalam persegi dengan panjang sisi 1 dan berpusat di (0,0)
points = generateRandomSquarePoints(100, 1)
print(points[10:15])
# CEK OUTPUT KODE ANDA VISUALISASI
# Mari kita Visualisasikan
# Jika sama dengan gambar di bawah ini maka keluaran sesuai harapan
x, y = zip(*points)

# persegi dengan panjang sisi 1 dan berpusat di (0,0)
r1 = plt.Rectangle((-0.5, -0.5), 1, 1, color='r', fill=False)
c1 = plt.Circle((0, 0), 0.5, color='b', fill=False)
fig, ax = plt.subplots(figsize=(9, 9))
ax.add_artist(r1)
ax.add_artist(c1)
plt.xlim(-0.6, 0.6)
plt.ylim(-0.6, 0.6)
plt.scatter(x, y, s=100, marker='x')
plt.show()
