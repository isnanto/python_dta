# nama file p1.py
# Isikan email anda dan copy semua cell code yang dengan komentar #Graded

# untuk revisi dan resubmisi sebelum deadline
# silakan di resubmit dengan nilai variable priority yang lebih besar dari
# nilai priority submisi sebelumnya
# JIKA TIDAK ADA VARIABLE priority DIANGGAP priority=0
priority = 0

# netacad email cth: 'abcd@gmail.com'
email = 'kang.isnanto@gmail.com'

# copy-paste semua #Graded cells YANG SUDAH ANDA KERJAKAN di bawah ini

# 1. Graded


def letter_catalog(items, letter='A'):
    pass
    # MULAI KODEMU DI SINI
    lbuah = []
    for buah in items:
        if buah[0] == letter:
            lbuah.append(buah)
    return lbuah

# 2. Graded


def counter_item(items):
    pass
    # MULAI KODEMU DI SINI
    dbuah = {}
    for item in items:
        if item in dbuah:
            dbuah[item] += 1
        else:
            dbuah.update({item: 1})
    return dbuah

# 3. Graded


# dua variable berikut jangan diubah
fruits = ['Apple', 'Avocado', 'Banana', 'Blackberries', 'Blueberries',
          'Cherries', 'Date Fruit', 'Grapes', 'Guava', 'Jackfruit', 'Kiwifruit']
prices = [6, 5, 3, 10, 12, 7, 14, 15, 8, 7, 9]

# list buah
chart = ['Blueberries', 'Blueberries', 'Grapes', 'Apple', 'Apple',
         'Apple', 'Blueberries', 'Guava', 'Jackfruit', 'Blueberries', 'Jackfruit']

# MULAI KODEMU DI SINI
fruit_price = None
fruit_price = {}
for x in range(0, len(fruits)):
    fruit_price.update({fruits[x]: prices[x]})


def total_price(dcounter, fprice):
    pass
    # MULAI KODEMU DI SINI
    total = 0
    for item in dcounter:
        pass
        total += dcounter[item] * fprice[item]
    return total

# 4. Graded


def discounted_price(total, discount, minprice=100):
    pass
    # MULAI KODEMU DI SINI
    if total >= minprice:
        total = total - (discount/100 * total)
    return total

# 5. Graded


def print_summary(items, fprice):
    pass
    # MULAI KODEMU DI SINI
    items.sort()
    ditem = counter_item(items)
    for item in ditem:
        print(ditem[item], item, ":", ditem[item] * fprice[item])
    print()
    print('total  :', total_price(counter_item(items), fprice))
    print('discount price :', discounted_price(total_price(
        counter_item(chart), fruit_price), 10, minprice=100))
