# nama file p2.py
# Isikan email anda dan copy semua cell code yang dengan komentar #Graded

# untuk revisi dan resubmisi sebelum deadline
# silakan di resubmit dengan nilai variable priority yang lebih besar dari
# nilai priority submisi sebelumnya
# JIKA TIDAK ADA VARIABLE priority DIANGGAP priority=0
import random
import math
priority = 0

# netacad email cth: 'abcd@gmail.com'
email = 'kang.isnanto@gmail.com'

# copy-paste semua #Graded cells YANG SUDAH ANDA KERJAKAN di bawah ini


def isPointInCircle(x, y, r, center=(0, 0)):
    if ((x-center[0])**2) + ((y-center[1])**2) > (r**2):
        return False
    else:
        return True


def generateRandomSquarePoints(n, length, center=(0, 0)):
    # MULAI KODEMU DI SINI
    minx = center[0]-length/2
    miny = center[1]-length/2

    # Gunakan list comprehension dengan variable minx, miny, length, dan n
    return [[random.uniform(minx, minx + length), random.uniform(miny, miny + length)] for x in range(n)]


def MCCircleArea(r, n=100, center=(0, 0)):
    # MULAI KODEMU DI SINI
    panjang = 2 * r
    titiktengah = 0
    for x, y in generateRandomSquarePoints(n, panjang, center):
        if (isPointInCircle(x, y, r, center)) == True:
            titiktengah += 1
            luas = (titiktengah/n)*(panjang**2)
    return luas


def LLNPiMC(nsim, nsample):
    # MULAI KODEMU DI SINI
    data = 0
    for x in range(nsim):
        data += (MCCircleArea(1, nsample))
    return data/nsim

# Graded


def relativeError(act, est):
    # MULAI KODEMU DI SINI
    data = ((est-act)/act)*100
    return abs(data)
