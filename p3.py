# nama file p3.py
# Isikan email anda dan copy semua cell code yang dengan komentar #Graded

# untuk revisi dan resubmisi sebelum deadline
# silakan di resubmit dengan nilai variable priority yang lebih besar dari
# nilai priority submisi sebelumnya
# JIKA TIDAK ADA VARIABLE priority DIANGGAP priority=0

import math
priority = 0

# netacad email cth: 'abcd@gmail.com'
email = 'kang.isnanto@gmail.com'

# copy-paste semua #Graded cells YANG SUDAH ANDA KERJAKAN di bawah ini

# Graded


def caesar_encript(txt, shift):
    result = ""

    for i in range(len(txt)):
        char = txt[i]
        if (char.isalpha()):

            if (char.isupper()):
                result += chr((ord(char) + shift - 65) % 26 + 65)

            else:
                result += chr((ord(char) + shift - 97) % 26 + 97)
        else:
            result += char
    return result

# Fungsi Decript caesar


def caesar_decript(chiper, shift):
    result = ""

    for i in range(len(chiper)):
        char = chiper[i]
        if (char.isalpha()):

            if (char.isupper()):
                result += chr((ord(char) - shift - 65) % 26 + 65)
            else:
                result += chr((ord(char) - shift - 97) % 26 + 97)
        else:
            result += char
    return result


msg = 'Haloz DTS mania, MANTAPSZZZ!'
cpr = caesar_encript(msg, 4)
txt = caesar_decript(cpr, 4)

print('plain text:', txt)
print('chiper text:', cpr)

# Graded

# Fungsi mengacak urutan


def shuffle_order(txt, order):
    return ''.join([txt[i] for i in order])

# Fungsi untuk mengurutkan kembali sesuai order


def deshuffle_order(sftxt, order):
    pass
    # Mulai Kode anda di sini
    return shuffle_order(sftxt, [order.index(i) for i in range(len(order))])


print(shuffle_order('abcd', [2, 1, 3, 0]))
print(deshuffle_order('cbda', [2, 1, 3, 0]))


def send_batch(txt, batch_order, shift=3):
    pass
    # Mulai Kode anda di sini
    acak = caesar_encript(txt, shift)
    lpecah = [acak[i:i+len(batch_order)]
              for i in range(0, len(acak), len(batch_order))]
    if(len(lpecah[len(lpecah)-1]) < len(batch_order)):
        baru = lpecah[len(lpecah)-1]
        baru = baru + ("_" * (len(batch_order)-len(baru)))
        lpecah.pop(-1)
        lpecah.append(baru)

    # Suffle

    supled = [shuffle_order(lpecah[i], batch_order)
              for i in range(len(lpecah))]

    return supled


def receive_batch(batch_cpr, batch_order, shift=3):
    batch_txt = [caesar_decript(deshuffle_order(
        i, batch_order), shift) for i in batch_cpr]
    return ''.join(batch_txt).strip('_')


msg_cpr = send_batch('halo DTS mania, mantaaap!!!', [2, 1, 3, 0], 4)
msg_txt = receive_batch(msg_cpr, [2, 1, 3, 0], 4)
print(msg_txt, msg_cpr, sep='\n')
