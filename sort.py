# https://www.youtube.com/watch?v=YHm_4bVOe1s
def bublesort(myList):
    for i in range(0, len(myList)-1):
        for j in range(0, len(myList)-1-i):
            if myList[j] > myList[j+1]:
                myList[j], myList[j+1] = myList[j+1], myList[j]
    return myList

# https://www.youtube.com/watch?v=Vca808JTbI8


def bublesortjuga(myList):
    for i in range(len(myList)-1, 0, -1):
        for j in range(i):
            if myList[j] > myList[j+1]:
                myList[j], myList[j+1] = myList[j+1], myList[j]

    return myList

# official


def bublesortwhile(myList):
    swapped = True  # it's a little fake - we need it to enter the while loop
    while swapped:
        swapped = False  # no swaps so far
        for i in range(len(myList) - 1):
            if myList[i] > myList[i + 1]:
                swapped = True  # swap occured!
                myList[i], myList[i + 1] = myList[i + 1], myList[i]
    return myList


# Aplikasi ==============================
nums = [5, 3, 8, 6, 7, 2]

# sort(nums)
nums = bublesortwhile(nums)
print(nums)

# builtin function
nums.sort(reverse=True)
print(nums)
nums.reverse()
print(nums)
nums.reverse()
print(nums)
