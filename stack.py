class stack:
    def __init__(self):
        self.stacklist = []

    def tambahkan(self, nilai):
        self.stacklist.append(nilai)

    def hapus(self):
        val = self.stacklist[-1]
        del self.stacklist[-1]
        return val

    def hapuspop(self):
        val = self.stacklist[0]
        self.stacklist.pop()
        return val


# obj = stack()
# print(len(obj.stacklist))
