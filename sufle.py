def shuffle_order(plain_text, order):
    return ''.join([plain_text[i] for i in order])


def de_shuffle_order(text_rand, order):
    d = dict(zip(text_rand, order))
    return ''.join(sorted(text_rand, key=lambda k: d[k]))


def deshuffle_order(sftext, order):
    pass
    b = ''
    for i in order:
        b += sftext[i]

    return b


# print(shuffle_order('abcd', [2, 1, 3, 0]))
#print(de_shuffle_order('cbda', [2, 1, 3, 0]))
print(shuffle_order('cbda', [2, 1, 3, 0]))

print(shuffle_order('tPshtsinayo', [1, 9, 0, 3, 10, 7, 6, 5, 4, 8, 2]))
print(shuffle_order('mcnlydnaeOA ie', [
      9, 6, 3, 12, 2, 13, 11, 10, 1, 7, 5, 8, 0, 4]))
