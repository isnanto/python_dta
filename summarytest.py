# 1 (zero)
# lst = [i for i in range(-1, -2)]
# print(lst)

# 2 fun(b=1)
# def fun(a, b, c=0):
#     print(a)
# print(fun(a=0, b=0, c=0))

# 3 (0 1)
# a = 1
# b = 0
# a = a ^ b
# b = a ^ b
# a = a ^ b
# print(a, b)

# 4 (same list, diferent name)
# nums = [1, 2, 3]
# vals = nums
# del vals[:]
# print(vals, nums)

# 5  (21)
# dct = {}
# dct['1'] = (1, 2)
# dct['2'] = (2, 1)

# for x in dct.keys():
#     print(dct[x][1], end="")

# 6 (4)
# tup = (1, 2, 4, 8)
# tup = tup[-2:-1]
# tup = tup[-1]
# print(tup)

# 7 (in)
# in = [1, 2, 3]
# print(in)

# 8 (36)
# x = input()
# y = input()
# print(x+y)

# 9 (1,1,1,2)
# lst = [1, 2]
# for v in range(2):
#     lst.insert(-1, lst[v])

# print(lst)

# 10 (0)
# print(1//2)

# 11 (0,1,4,9)
# list = [x * x for x in range(5)]


# def fun(lst):
#     del lst[lst[2]]
#     return lst


# print(fun(list))

# 12  (2)
# def fun(x):
#     if x % 2 == 0:
#         return 1
#     else:
#         return 2
# print(fun(fun(2)))

# 13 not equal (!=)

# 14 (Infinite Loop) (tes pertama salah)
# i = 0
# while i < i + 2:
#     i += 1
#     print("*")
# else:
#     print("*")

# 15 (error)
# def func(a, b):
#     return b**a
# print(func(b=2, 2))

# 16 positional argument. its posistion with argument list / argument name specified along with its value

# 17 (ERROR NO VALS METHODE)
# dd = {"1": "0", "0": "1"}
# for x in dd.vals():
#     print(x, end="")

# 18 (THREE)
# lst = [[x for x in range(3)] for y in range(3)]
# for r in range(3):
#     for c in range(3):
#         if lst[r][c] % 2 != 0:
#             print("#")

# 19 (0.2)
# x = 1 // 5 + 1 / 5
# print(x)

# 20 (0)
# x = int(input())
# y = int(input())
# x = x % y
# x = x % y
# y = y % x
# print(y)

# 21 (different name same list)
# nums = [1, 2, 3]
# vals = nums
# print(nums, vals)

# 22 (run time error  Nonne type)
# def func1(a):
#     return None
# def func2(a):
#     return func1(a) * func1(a)
# print(func2(2))

# 23 (2.0)
# print(float(4) ** (1/float(2)))

# 24 (true)
# z = 0
# y = 10
# print(y < z and z > y or y > z and z < y)

# 25 (1 1 2)
# x = 1
# y = 2
# x, y, z = x, x, y
# z, y, z = x, y, z
# print(x, y, z)

# 26 (illegal, tuple not support assignment)
# tuple = (1, 1, 1, 1)
# tuple[1] = tuple[1] + tuple[0]
# print(tuple)

# 27 (asepbsepc)
# print("a", "b", "c", sep="sep")

# 28 (one)
# dct = {"one": "two", 'three': 'one', 'two': 'three'}
# v = dct['three']
# for k in range(len(dct)):
#     v = dct[v]
# print(v)

# 29 (4)
# def fun(inp=2, out=3):
#     return inp * out
# print(fun(out=2))

# 30 (0)
# def fun(x, y):
#     if x == y:
#         return x
#     else:
#         return fun(x, y-1)
# print(fun(0, 3))
