# Graded

import math


def caesar_encript(txt, shift):
    result = ""

    for i in range(len(txt)):
        char = txt[i]
        if (char.isalpha()):

            if (char.isupper()):
                result += chr((ord(char) + shift - 65) % 26 + 65)

            else:
                result += chr((ord(char) + shift - 97) % 26 + 97)
        else:
            result += char
    return result


def caesar_decript(chiper, shift):
    return caesar_encript(chiper, -shift)


def shuffle_order(txt, order):
    return ''.join([txt[i] for i in order])

# Fungsi untuk mengurutkan kembali sesuai order


def deshuffle_order(sftxt, order):
    pass
    # Mulai Kode anda di sini
    return shuffle_order(sftxt, [order.index(i) for i in range(len(order))])


def de_shuffle_order(text_rand, order):
    d = dict(zip(text_rand, order))
    return ''.join(sorted(text_rand, key=lambda k: d[k]))


# print(shuffle_order('abcd', [2, 1, 3, 0]))
# print(deshuffle_order('cbda', [2, 1, 3, 0]))


# convert txt ke dalam bentuk list teks yang lebih pendek
# dan terenkrispi dengan urutan acak setiap batchnya


def send_batch(txt, batch_order, shift=3):
    pass
    # Mulai Kode anda di sini
    acak = caesar_encript(txt, shift)
    lpecah = [acak[i:i+len(batch_order)]
              for i in range(0, len(acak), len(batch_order))]
    if(len(lpecah[len(lpecah)-1]) < len(batch_order)):
        baru = lpecah[len(lpecah)-1]
        baru = baru + ("_" * (len(batch_order)-len(baru)))
        lpecah.pop(-1)
        lpecah.append(baru)

    # Suffle

    supled = [shuffle_order(lpecah[i], batch_order)
              for i in range(len(lpecah))]
    # lpecah.append(baru)
    # print(lpecah[len(lpecah)-1])
    # print("Acak ", acak)
    # print("Supled : ", supled)
    return supled


# batch_cpr: list keluaran send_batch
# fungsi ini akan mengembalikan lagi ke txt semula
def receive_batch(batch_cpr, batch_order, shift=3):
    batch_txt = [caesar_decript(de_shuffle_order(
        i, batch_order), shift) for i in batch_cpr]
    return ''.join(batch_txt).strip('_')


# print(send_batch("halo dts mania", [2, 1, 3, 0], 4))
#msg_cpr = send_batch('halo DTS mania, mantaaap!!!', [2, 1, 3, 0], 4)
msg_cpr = send_batch('MaroRiver.PythonAnywhere.COM', [2, 0, 3, 6, 5, 1, 4], 10)
msg_txt = receive_batch(msg_cpr, [2, 1, 3, 0], 4)
print(msg_txt, msg_cpr, sep='\n')
