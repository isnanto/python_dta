import math
import random


def isPointInCircle(x, y, r, center=(0, 0)):
    if ((x-center[0])**2) + ((y-center[1])**2) > (r**2):
        return False
    else:
        return True


def generateRandomSquarePoints(n, length, center=(0, 0)):
    # MULAI KODEMU DI SINI
    minx = center[0]-length/2
    miny = center[1]-length/2

    # Gunakan list comprehension dengan variable minx, miny, length, dan n
    points = [[random.uniform(
        minx, minx + length), random.uniform(miny, miny + length)] for x in range(n)]

    return points


def MCCircleArea(r, n=100, center=(0, 0)):
    # MULAI KODEMU DI SINI
    length = 2*r
    titiktengah = 0
    for x, y in generateRandomSquarePoints(n, length, center):
        if (isPointInCircle(x, y, r, center)) == True:
            titiktengah += 1
            luas = (titiktengah/n)*(length**2)
    return luas


def LLNPiMC(nsim, nsample):
    # MULAI KODEMU DI SINI
    data = 0
    for x in range(nsim):
        data += (MCCircleArea(1, nsample))
    return data/nsim

# Graded


def relativeError(act, est):
    # MULAI KODEMU DI SINI
    data = ((est-act)/act)*100
    return abs(data)


# CEK OUTPUT KODE ANDA
# import math


random.seed(0)
estpi = LLNPiMC(10000, 500)

print('est_pi:', estpi)
print('act_pi:', math.pi)

print('error relatif:', relativeError(math.pi, estpi), '%')
